const gitlabRoutes = require("./gitlab")
const gitpodRoutes = require("./gitpod")
const mattermostRoutes = require("./mattermost")
const { getTotalMonsters } = require("../../db")
const process = require("process")

/**
 * Returns formatted uptime of process
 */
function determineUptime () {
  const formatUptime = (sec) => {
    const pad = (s) => {
      return (s < 10 ? '0' : '') + s
    }

    const days = Math.floor(sec / (60 * 60 * 24))
    const hours = Math.floor(sec / (60 * 60))
    const minutes = Math.floor(sec % (60 * 60) / 60)
    const seconds = Math.floor(sec % 60)

    return `${days ? days + 'd ' : ''}${pad(hours % ((days || 1) * 24))}h${pad(minutes)}m${pad(seconds)}s`
  }

  return formatUptime(process.uptime())
}

/**
 * Returns formatted memory usage of process
 */
function determineMemory () {
  const formatMemory = (data) => `${(data / 1024 / 1024).toFixed(2)} MB`

  const memoryData = process.memoryUsage()
  return formatMemory(memoryData.rss)
}

/**
 * Returns formatted processor usage of process
 */
function determineProcessor () {
  const determineProcessor = () => {
    const usage = Object.assign({}, process.cpuUsage())
    usage.time = process.uptime() * 1000 // seconds to milliseconds
    usage.percent = (usage.system + usage.user) / (usage.time * 10) // microseconds to milliseconds
    return usage
  }

  return determineProcessor().percent.toFixed(2) + '%'
}

module.exports = {
  name: "v1",
  register: async function (server, options) {
    server.route([
      {
        method: "GET",
        path: "/",
        handler: (request, h) => {
          return `Mattermonsters API v1`
        },
      },
      {
        method: "GET",
        path: "/status",
        handler: async (request, h) => {
          return {
            up: true,
            totalMonsters: await getTotalMonsters(),
            uptime: determineUptime(),
            memory: determineMemory(),
            cpu: determineProcessor() 

          }
        },
      },
      ...gitlabRoutes,
      ...gitpodRoutes,
      ...mattermostRoutes,
    ])
  },
}
