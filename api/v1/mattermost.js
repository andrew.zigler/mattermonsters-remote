const fetch = require("node-fetch")
const { getUser, putMonster, getMonster } = require("../../db")
const fs = require('fs')
const crypto = require('crypto')
const AWS = require('aws-sdk')

module.exports = [
  {
    method: "POST",
    path: "/mattermost/ping",
    handler: async (request, h) => {
      const branch = request.payload.user_name
      const feature = request.payload.text

      const user = await getUser(branch)
      const gitpod = user.gitpod

      const features = ["body", "eyes", "mouth", "clothing", "accessory"]

      const tableTop = "#### Checking your merged branches on Gitpod... \n| Branch name                | Result                           |\n|:--------------------|:---------------------------------|\n"
      let tableFeats = ''

      for (const feat of features) {
        const pong = await fetch(`https://8080-${gitpod}/${feat}`)

        tableFeats = tableFeats + `| \`${feat}\`     | ${pong.status === 200 ? '🟢' : '❌'} |\n`
      }

      return {
        response_type: "in_channel",
        text: `${tableTop}${tableFeats}\n`,
      }
    }
  },
  {
    method: "POST",
    path: "/mattermost/release",
    config: {
      cors : true,
      auth: false,
      payload: {
        maxBytes: 209715200,
        output: 'file',
        parse: true,
        multipart: true
      }
    },
    handler: async (request, h) => {
      const data = request.payload
      const _id = data.monsterId

      console.log(data)

      let base64String = data.monsterPolaroid
      let base64Image = base64String.split(';base64,').pop();

      putMonster({
        _id,
        ...data
      })

      fs.writeFile(`monsters/${_id}.png`, base64Image, {encoding: 'base64'}, async (err) => {

        const s3 = new AWS.S3({
          accessKeyId: process.env.AWS_ACCESS,
          secretAccessKey: process.env.AWS_SECRET
        })

        const blob = fs.readFileSync(`monsters/${_id}.png`)

        // upload to S3
        const filename = _id

        const params = {
          Bucket: process.env.AWS_BUCKET,
          Key: `${filename}.png`,
          Body: blob,
          ContentType: 'image/png'
        }

        s3.upload(params, (err, awsData) => {
          if (err) {
            return console.log("error uploading...", err)
          }
          console.log(`File created: ${awsData.Location}`)

          // cloud server
          fetch(process.env.CLOUD_WEBHOOK, {
            method: "POST",
            body: JSON.stringify({ attachments:[{
              title: `Watch out — another Mattermonster named ${data.monsterName} was released into the wild! We snapped a pic 📸 before the monster scurried away.`,
              color: "#1e325c",
              image_url: awsData.Location
            } ]}),
            headers: {'Content-Type': 'application/json'}
          })

          // community server
          fetch(process.env.COMMUNITY_WEBHOOK, {
            method: "POST",
            body: JSON.stringify({ attachments:[{
              title: `Watch out — another Mattermonster named ${data.monsterName} was released into the wild! We snapped a pic 📸 before the monster scurried away.`,
              color: "#1e325c",
              image_url: awsData.Location
            } ]}),
            headers: {'Content-Type': 'application/json'}
          })
        })

        // console.log(await getMonster(_id))
      });

    return ""

      /*

      TODO: set profile picture
      https://api.mattermost.com/#tag/users/operation/SetProfileImage

      const user = await getUser(branch)
      const gitpod = user.gitpod

      const pong = await fetch(`https://8080-${gitpod}/${feature}`).catch(
        (e) => {
          console.log(e)
        }
      )

      if (pong.status === 200) {
        return {
          response_type: "in_channel",
          text: `Success! Received response \`200\` from \`${feature}\` on your Gitpod!`,
        }
      } else {
        return {
          response_type: "in_channel",
          text: `Error: No response received from \`${feature}\` on your Gitpod...`,
        }
      }
      */
    },
  },
]
