const { putUser } = require("../../db")

module.exports = [
  {
    method: "POST",
    path: "/gitpod",
    handler: async (request, h) => {
      return {
        response_type: "in_channel",
        text: `Access your Gitpod cloud workstation: https://gitpod.io/#https://gitlab.com/andrew.zigler/mattermonsters/-/tree/${request.payload.user_name}/\n\n⚛️ The React project will automatically start building and will open once it's ready. **After that happens, come back here and follow the next step.**`,
      }
    },
  },
  {
    method: "PUT",
    path: "/gitpod/ping/{branch}/{gpurl}",
    options: {
      auth: false,
    },
    handler: async (request, h) => {
      const _id = request.params.branch
      const gitpod = request.params.gpurl

      putUser({
        _id,
        gitpod,
      })

      return h.response().code(201)
    },
  },
]
