const fetch = require("node-fetch")

const gitLabRepositoryAPI = `${process.env.GITLAB_API_URL}/projects/${process.env.GITLAB_PROJECT_ID}`
const gitLabBranchesAPI = `${gitLabRepositoryAPI}/repository/branches`
const gitLabMergeRequestsAPI = `${gitLabRepositoryAPI}/merge_requests`

const { getUser, putUser } = require("../../db")

module.exports = [
  {
    method: "POST",
    path: "/gitlab/init",
    handler: async (request, h) => {
      const branch_name = request.payload.user_name

      const response = await fetch(
        `${gitLabBranchesAPI}?branch=${branch_name}&ref=main`,
        {
          headers: { Authorization: `Bearer ${process.env.GITLAB_TOKEN}` },
          method: "POST",
        }
      ).catch((e) => {
        console.log(e)
      })

      const result = await response.json()
      console.log(result)

      if (result.message === "Branch already exists") {
        return {
          response_type: "in_channel",
          text: `Looks like you already have a branch: https://gitlab.com/andrew.zigler/mattermonsters/-/tree/${branch_name}`,
        }
      } else {
        return {
          response_type: "in_channel",
          text: `Your branch was created: https://gitlab.com/andrew.zigler/mattermonsters/-/tree/${branch_name}\n\nYou can fetch your Gitpod cloud workspace URL with \`\`\`\n/gitpod\`\`\`\n`,
        }
      }
    },
  },
  {
    method: "POST",
    path: "/gitlab/merge",
    handler: async (request, h) => {
      const feature = request.payload.text
      const responseUrl = request.payload.response_url

      const _id = request.payload.user_name
      const user = await getUser(_id)

      if (user.merging) {
        return {
          response_type: "in_channel",
          text: "You are already merging, please wait...",
        }
      }

      const featureExistsResponse = await fetch(
        `${gitLabBranchesAPI}/${feature}`
      ).catch((e) => {
        console.log(e)
      })

      const featureExistsResult = await featureExistsResponse.json()
      console.log(featureExistsResult)

      if (featureExistsResult.message === "404 Branch Not Found") {
        return {
          response_type: "in_channel",
          text: `The \`${feature}\` branch does not exist!`,
        }
      }

      const mergeRequestResponse = await fetch(
        `${gitLabMergeRequestsAPI}?source_branch=${feature}&target_branch=${request.payload.user_name}&title="Merge ${feature} into ${request.payload.user_name}"`,
        {
          headers: { Authorization: `Bearer ${process.env.GITLAB_TOKEN}` },
          method: "POST",
        }
      ).catch((e) => {
        console.log(e)
      })

      const mergeRequestResult = await mergeRequestResponse.json()
      console.log(mergeRequestResult)
      let iid

      if (
        mergeRequestResult.message &&
        Array.isArray(mergeRequestResult.message)
      ) {
        if (
          mergeRequestResult.message[0].includes(
            "Another open merge request already exists for this source branch: "
          )
        ) {
          iid = mergeRequestResult.message[0].split("!")[1]
        }
      } else {
        iid = mergeRequestResult.iid
      }

      const tryMerge = async () => {
        console.log(`${gitLabMergeRequestsAPI}/${iid}/merge`)
        const response = await fetch(`${gitLabMergeRequestsAPI}/${iid}/merge`, {
          headers: { Authorization: `Bearer ${process.env.GITLAB_TOKEN}` },
          method: "PUT",
        }).catch((e) => {
          console.log(e)
        })

        const result = await response.json()

        user.merging = false
        console.log("done merging...")
        putUser(user)

        if (result.message === "405 Method Not Allowed") {
          return {
            response_type: "in_channel",
            text: `An error occured when merging your request. Have you already merged the \`${feature}\` branch into your branch?`,
          }
        } else {
          return result
        }
      }

      let mergeResult
      setTimeout(async () => {
        mergeResult = await tryMerge()

        if (mergeResult.text) {
          await fetch(responseUrl, {
            method: "POST",
            body: mergeResult.text,
          })
        } else {
          await fetch(responseUrl, {
            method: "POST",
            body: `The \`${feature}\` branch was successfully merged. Don't forget to pull down your changes on Gitpod!`,
          })
        }
      }, 1000 * 5)

      user.merging = true
      console.log("got this merger...", user)
      putUser(user)

      return {
        response_type: "ephemeral",
        text: "Merging, please wait...",
      }
    },
  },
]
