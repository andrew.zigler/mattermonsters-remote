const pouch = require("pouchdb")
const userDB = new pouch("db/user")
const monsterDB = new pouch("db/monster")

const put = (db, doc) => {
  db.put(doc)
    .then(function (doc) {
      console.log(`==> ${db.name} was updated`)
    })
    .catch(function (err) {
      db.get(doc._id)
        .then(function (old) {
          doc._rev = old._rev
          db.put(doc)
          console.log(`==> ${db.name} was updated`)
        })
        .catch(function (err) {
          console.log(`ERROR: ${db.name} could not save`)
        })
    })
}

const get = (db, doc) => {
  return db
    .get(doc)
    .then(function (doc) {
      return doc
    })
    .catch(function (err) {
      return { _id: null, message: "document does not exist" }
    })
}

const putUser = (doc) => {
  put(userDB, doc)
}

const getUser = async (doc) => {
  return await get(userDB, doc)
}

const putMonster = (doc) => {
  put(monsterDB, doc)
}

const getMonster = async (doc) => {
  return await get(monsterDB, doc)
}

const getTotalMonsters = async () => {
  return (await monsterDB.info()).doc_count
}

module.exports = { put, putUser, getUser, putMonster, getMonster, getTotalMonsters }
