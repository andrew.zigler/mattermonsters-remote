const ipLib = require("ip")
const unauthorized = require("@hapi/boom").unauthorized

function matches(ip, remoteAddress) {
  if (ipLib.isV4Format(ip) || ipLib.isV6Format(ip)) {
    return ipLib.isEqual(ip, remoteAddress)
  }

  if (ip.includes("/")) {
    return ipLib.cidrSubnet(ip).contains(remoteAddress)
  }

  return false
}

function scheme(server, options) {
  return {
    authenticate: (request, h) => {
      const { remoteAddress } = request.info

      const list = ["192.168.0.0/16", "172.177.0.0/24", process.env.DEV_IP]

      //if (list.some((ip) => matches(ip, remoteAddress))) {
        return h.authenticated({ credentials: remoteAddress })
      //}

      throw unauthorized(`${remoteAddress} is not a valid IP`)
    },
  }
}

module.exports = { scheme }
