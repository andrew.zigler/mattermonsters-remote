require("dotenv").config()
const fs = require('fs')

// currently using /api/v1
const currentAPI = process.env.CURRENT_API
const Hapi = require("@hapi/hapi")
const v1Api = require("." + currentAPI)

const { scheme } = require("./auth")

const init = async () => {
  const server = Hapi.server({
    port: 4000,
    router: { stripTrailingSlash: true },
    tls: {
      key: fs.readFileSync(process.env.PRIVKEY),
      cert: fs.readFileSync(process.env.FULLCHAIN)
    }
  })

  await server.register(v1Api, {
    routes: {
      prefix: currentAPI,
    },
  })

  // configure IP whitelist for server
  server.auth.scheme("scheme", scheme)
  server.auth.strategy("whitelist", "scheme")
  server.auth.default("whitelist")

  await server.start()
  console.log(`👹 API available @ ${server.info.uri}${currentAPI}`)
}

process.on("unhandledRejection", (err) => {
  console.log(err)
  process.exit(1)
})

init()
